import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'
import Table from '../views/Table.vue'
import Search from '../views/Search.vue'
import About from '../views/About.vue'
import Demo from '../views/Demo.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home
  },
  {
    path: '/Table',
    name: 'Table',
    component: Table
  },
  {
    path: '/Search',
    name: 'Search',
    component: Search
  },
  {
    path: '/About',
    name: 'About',
    component: About
  },
  {
    path: '/Demo',
    name: 'Demo',
    component: Demo
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
