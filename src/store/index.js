import Vue from 'vue'
import Vuex from 'vuex'
import router from '../router'

Vue.use(Vuex)
const axios = require('axios').default

export default new Vuex.Store({
  state: {
    server: 'http://localhost:5000/api',
    segments: [],
    similarWords: [],
    status: 'No data loaded',
    tmp:
      '[{"id": 0, "full_segment": "My name is Boris I like candy and beer. Also I like small little kitties. Butt what can i do?! im a man after all.", "child_count": "4", "children": [{"id": 0, "content": "My name is Boris I like candy and beer.", "ranks": [0.023806221783161163, 0.01341647282242775, 0.014215760864317417, 0.9485615491867065]}, {"id": 1, "content": "Also I like small little kitties.", "ranks": [0.7072219848632812, 0.10579541325569153, 0.038179878145456314, 0.14880280196666718]}, {"id": 2, "content": "Butt what can i do?!", "ranks": [0.9961538910865784, 0.0010759395081549883, 0.0013189426390454173, 0.0014512271154671907]}, {"id": 3, "content": "im a man after all.", "ranks": [0.034182384610176086, 0.07632659375667572, 0.012349084950983524, 0.8771418929100037]}]}, {"id": 1, "full_segment": "My name is Pasha and fuck you all", "child_count": "1", "children": [{"id": 4, "content": "My name is Pasha and fuck you all", "ranks": [0.015615012496709824, 0.0053115421906113625, 0.010396911762654781, 0.9686765074729919]}]}]'
  },
  mutations: {
    mutSegments: (state, RankedData) => {
      // state.segments = jsonToTree(JSON.parse(state.tmp), state) // DELETEME
      state.segments = jsonToTree(RankedData, state)
      state.status = 'Data has been loaded successfully'
      router.push('Table')
    },
    mutSimilarWords: (state, similarWords) => {
      // state.segments = jsonToTree(JSON.parse(state.tmp), state) // DELETEME
      state.similarWords = similarWords
    }
  },
  actions: {
    async makeApiRequest ({ commit, state }, parameters) {
      var requestType = parameters[0]
      var rawBody = parameters[1]
      state.status = 'API request has been sent'
      var resp = await axios
        .post(state.server, rawBody, {
          headers: {
            'content-type': 'application/json'
          }
        })
        .then(function (val) {
          return val.data
        })
      state.status = 'API Resonse has been received'
      if (requestType === 'similarity') {
        if (resp.neighbors.length < 1) state.status = 'Search Results Are Ready'
        commit('mutSimilarWords', resp)
        console.log('Similar Words: ' + resp.neighbors)
      } else {
        commit('mutSegments', resp)
        // console.log('Classification Results: ' + resp)
      }
      // commit('mutSegments', 'xyz')
    },
    searchTree ({ commit, state }, searchArray) {
      state.status = 'Processing Search Results'
      var searchResults = []
      for (var seg of state.segments) {
        for (var sent of seg.sentences) {
          for (var word of searchArray) {
            var tmp = sent.content.toLowerCase()
            if (tmp.includes(word.toLowerCase())) {
              delete sent.sid
              searchResults.push(sent)
              break
            }
          }
        }
      }
      state.status = 'Search Results Are Ready'
      return searchResults
    }
  }
})

function jsonToTree (RankedData, state) {
  state.status = 'Ordering API response to structured format'
  const segments = []
  for (var seg of RankedData) {
    const newSeg = new Segments(seg)
    for (var sent of seg.children) {
      newSeg.addSentence(newSeg, sent)
    }
    segments.push(newSeg)
  }
  state.status = 'Structured format is ready to use'
  return segments
}

class Segments {
  constructor (segment) {
    this.fullsegment = segment.full_segment
    this.id = segment.id
    this.sentences = []
  }

  addSentence (self, sentence) {
    var newSent = new Sentence(self, sentence)
    this.sentences.push(newSent)
  }

  addPrepreparedSentence (sent) {
    this.sentences.push(sent)
  }
}

class Sentence {
  constructor (seg, sentence) {
    this.content = sentence.content
    this.sid = sentence.id
    this.ranks = sentence.ranks
    this.segment = seg
    this.category = sentence.category
    this.splitRanks()
  }

  splitRanks () {
    this.rankNothing = this.ranks[0]
    this.rankOpinions = this.ranks[1]
    this.rankEvironment = this.ranks[2]
    this.rankFacts = this.ranks[3]
    delete this.ranks
  }
}
